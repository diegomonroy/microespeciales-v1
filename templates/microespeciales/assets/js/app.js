// JavaScript Document

/* ************************************************************************************************************************

Microespeciales

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {

	/* Menus */

	var path = 'images/microespeciales/pdf/';
	var file_01 = path + 'pdf_01.pdf';
	var file_02 = path + 'pdf_02.pdf';
	var file_03 = path + 'pdf_03.pdf';
	var file_04 = path + 'pdf_04.pdf';
	var file_05 = path + 'pdf_05.pdf';
	var file_06 = path + 'pdf_06.pdf';
	var file_07 = path + 'pdf_07.pdf';
	var file_08 = path + 'pdf_08.pdf';
	var file_09 = path + 'pdf_09.pdf';
	var file_10 = path + 'pdf_10.pdf';
	var file_11 = path + 'pdf_11.pdf';

	// PDF 01
	jQuery( 'li.item-112' ).html( '<a data-fancybox data-src="' + file_01 + '" href="javascript:;">Metalografía, Polarización, Confocal</a>' );
	jQuery( 'li.item-113' ).html( '<a data-fancybox data-src="' + file_01 + '" href="javascript:;">Microscopios Opto Digitales</a>' );
	jQuery( 'li.item-114' ).html( '<a data-fancybox data-src="' + file_01 + '" href="javascript:;">Estéreo Microscopios</a>' );
	jQuery( 'li.item-115' ).html( '<a data-fancybox data-src="' + file_01 + '" href="javascript:;">Cámaras Digitales</a>' );
	jQuery( 'li.item-116' ).html( '<a data-fancybox data-src="' + file_01 + '" href="javascript:;">Software de Análisis de Imágenes</a>' );
	// PDF 02
	jQuery( 'li.item-117' ).html( '<a data-fancybox data-src="' + file_02 + '" href="javascript:;">ANALIZADORES XRF y XRD OLYMPUS</a>' );
	// PDF 03
	jQuery( 'li.item-119' ).html( '<a data-fancybox data-src="' + file_03 + '" href="javascript:;">Micro Semi-micro Analíticas de precisión Grameras plataformas</a>' );
	jQuery( 'li.item-120' ).html( '<a data-fancybox data-src="' + file_03 + '" href="javascript:;">Contadoras</a>' );
	jQuery( 'li.item-121' ).html( '<a data-fancybox data-src="' + file_03 + '" href="javascript:;">VIBRO REOMETROS A&D</a>' );
	jQuery( 'li.item-122' ).html( '<a data-fancybox data-src="' + file_03 + '" href="javascript:;">VISCOSIMETROS A&D</a>' );
	jQuery( 'li.item-123' ).html( '<a data-fancybox data-src="' + file_03 + '" href="javascript:;">PIPETAS ELECTRONICAS A&D</a>' );
	// PDF 04
	jQuery( 'li.item-125' ).html( '<a data-fancybox data-src="' + file_04 + '" href="javascript:;">Multidisciplinario, Oftalmología</a>' );
	jQuery( 'li.item-126' ).html( '<a data-fancybox data-src="' + file_04 + '" href="javascript:;">Otorrinolaringología</a>' );
	jQuery( 'li.item-127' ).html( '<a data-fancybox data-src="' + file_04 + '" href="javascript:;">Odontología</a>' );
	jQuery( 'li.item-128' ).html( '<a data-fancybox data-src="' + file_04 + '" href="javascript:;">COLPOSCOPIOS</a>' );
	jQuery( 'li.item-129' ).html( '<a data-fancybox data-src="' + file_04 + '" href="javascript:;">LAMPARAS DE HENDIDURA</a>' );
	jQuery( 'li.item-130' ).html( '<a data-fancybox data-src="' + file_04 + '" href="javascript:;">CÁMARAS DIGITALES SOFTWARE</a>' );
	// PDF 05
	jQuery( 'li.item-132' ).html( '<a data-fancybox data-src="' + file_05 + '" href="javascript:;">Metalográficas y Petrográficas</a>' );
	jQuery( 'li.item-133' ).html( '<a data-fancybox data-src="' + file_05 + '" href="javascript:;">Cortadoras, Pulidoras, Embutidoras</a>' );
	jQuery( 'li.item-134' ).html( '<a data-fancybox data-src="' + file_05 + '" href="javascript:;">Durómetros, Micro durómetros</a>' );
	// PDF 06
	jQuery( 'li.item-136' ).html( '<a data-fancybox data-src="' + file_06 + '" href="javascript:;">Micrótomos, Criostatos, Baños de Flotación, Procesadores de tejidos, Cuchillas, Parafina, Centrales de inclusión, Anillos, Casetes, etc.</a>' );
	// PDF 07
	jQuery( 'li.item-138' ).html( '<a data-fancybox data-src="' + file_07 + '" href="javascript:;">MicroTec</a>' );
	// PDF 08
	jQuery( 'li.item-140' ).html( '<a data-fancybox data-src="' + file_08 + '" href="javascript:;">Estéreo microscopios Escolares</a>' );
	jQuery( 'li.item-141' ).html( '<a data-fancybox data-src="' + file_08 + '" href="javascript:;">Microscopios Escolares Digitales</a>' );
	jQuery( 'li.item-142' ).html( '<a data-fancybox data-src="' + file_08 + '" href="javascript:;">Estuches de Muestras Preparadas</a>' );
	// PDF 09
	jQuery( 'li.item-144' ).html( '<a data-fancybox data-src="' + file_09 + '" href="javascript:;">Micrómetros, Calibración</a>' );
	jQuery( 'li.item-145' ).html( '<a data-fancybox data-src="' + file_09 + '" href="javascript:;">Iluminaciones LED, Bombillos</a>' );
	jQuery( 'li.item-146' ).html( '<a data-fancybox data-src="' + file_09 + '" href="javascript:;">Objetivos, Oculares, Partes y Repuestos</a>' );
	jQuery( 'li.item-147' ).html( '<a data-fancybox data-src="' + file_09 + '" href="javascript:;">Estuches Para Guardar Muestras</a>' );
	// PDF 10
	jQuery( 'li.item-148' ).html( '<a data-fancybox data-src="' + file_10 + '" href="javascript:;">CENTRIFUGAS, AGITADORES, NEVERAS, FREEZERS, VORTEX, INCUBADORAS, BAÑOS TERMOSTATADOS</a>' );
	// PDF 11
	jQuery( 'li.item-149' ).html( '<a data-fancybox data-src="' + file_11 + '" href="javascript:;">CONSUMIBLES</a>' );
	jQuery( 'li.item-150' ).html( '<a data-fancybox data-src="' + file_11 + '" href="javascript:;">LUPAS GEOLOGICAS, GEMOLOGICAS LED</a>' );
	jQuery( 'li.item-151' ).html( '<a data-fancybox data-src="' + file_11 + '" href="javascript:;">BOMBILLERIA</a>' );
	jQuery( 'li.item-153' ).html( '<a data-fancybox data-src="' + file_11 + '" href="javascript:;">Mantenimiento y Reparación técnica</a>' );
	jQuery( 'li.item-154' ).html( '<a data-fancybox data-src="' + file_11 + '" href="javascript:;">ACCESORIOS PARTES Y REPUESTOS</a>' );
	jQuery( 'li.item-155' ).html( '<a data-fancybox data-src="' + file_11 + '" href="javascript:;">ASESORIA</a>' );

	/* Foundation */

	jQuery( '#nombres, #telefono, #celular, #email, #mensaje' ).foundation( 'destroy' );

});