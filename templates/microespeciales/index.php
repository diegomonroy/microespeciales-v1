<?php

/* ************************************************************************************************************************

Microespeciales

File:			index.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$class = $pageclass->get( 'pageclass_sfx' );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_banner = $this->countModules( 'banner' );
$show_bottom = $this->countModules( 'bottom' );
$show_left = $this->countModules( 'left' );
$show_logo = $this->countModules( 'logo' );
$show_menu = $this->countModules( 'menu' );
$show_news = $this->countModules( 'news' );
$show_product = $this->countModules( 'product' );
$show_service = $this->countModules( 'service' );
$show_top = $this->countModules( 'top' );

// Params

?>
<!DOCTYPE html>
<html class="no-js" lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="-va0MHyeEobIVMS7DMRJeFp-2YBv7RlXoLXJAtd8V7U">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>build/bower_components/foundation-sites/dist/css/foundation-flex.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/animate.css/animate.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/fancybox/dist/jquery.fancybox.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/app.css" rel="stylesheet" type="text/css">
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-74928858-1', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php if ( $show_top ) : ?>
		<!-- Begin Top -->
			<section class="top wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<jdoc:include type="modules" name="top" style="xhtml" />
				</div>
			</section>
		<!-- End Top -->
		<?php endif; ?>
		<?php if ( $show_menu ) : ?>
		<!-- Begin Menu -->
			<section class="menu-wrap wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="menu" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Menu -->
		<?php endif; ?>
		<!-- Begin Off-canvas -->
			<section class="component wow fadeIn" data-wow-delay="0.5s">
				<button type="button" class="button show-for-small-only" data-toggle="offCanvas">NUESTROS PRODUCTOS</button>
				<div class="off-canvas-wrapper">
					<div class="off-canvas-absolute position-left" id="offCanvas" data-off-canvas>
						<button type="button" class="close-button" aria-label="Cerrar" data-close><span aria-hidden="true">&times;</span></button>
						<div class="row align-center align-middle">
							<div class="small-12 columns">
								<?php if ( $show_left ) : ?>
								<!-- Begin Left -->
									<div class="left">
										<jdoc:include type="modules" name="left" style="xhtml" />
									</div>
								<!-- End Left -->
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="off-canvas-content" data-off-canvas-content>
						<div class="row align-center">
							<div class="small-12 medium-3 columns hide-for-small-only">
								<?php if ( $show_left ) : ?>
								<!-- Begin Left -->
									<div class="left">
										<jdoc:include type="modules" name="left" style="xhtml" />
									</div>
								<!-- End Left -->
								<?php endif; ?>
							</div>
							<div class="small-12 medium-9 columns">
								<?php if ( $show_banner ) : ?>
								<!-- Begin Banner -->
									<div class="banner">
										<jdoc:include type="modules" name="banner" style="xhtml" />
									</div>
								<!-- End Banner -->
								<?php endif; ?>
								<?php if ( $show_news ) : ?>
								<!-- Begin News -->
									<div class="news">
										<jdoc:include type="modules" name="news" style="xhtml" />
									</div>
								<!-- End News -->
								<?php else : ?>
								<!-- Begin Content -->
									<div class="content">
										<jdoc:include type="component" />
									</div>
								<!-- End Content -->
								<?php endif; ?>
								<?php if ( $show_product ) : ?>
								<!-- Begin Product -->
									<div class="product">
										<jdoc:include type="modules" name="product" style="xhtml" />
									</div>
								<!-- End Product -->
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		<!-- End Off-canvas -->
		<?php if ( $show_service ) : ?>
		<!-- Begin Service -->
			<section class="service wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="service" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Service -->
		<?php endif; ?>
		<?php if ( $show_logo ) : ?>
		<!-- Begin Logo -->
			<section class="logo wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="logo" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Logo -->
		<?php endif; ?>
		<?php if ( $show_bottom ) : ?>
		<!-- Begin Bottom -->
			<section class="bottom wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="bottom" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Bottom -->
		<!-- Begin Copyright -->
			<section class="copyright_wrap wow fadeIn" data-wow-delay="0.5s">
				<div class="copyright">
					&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
				</div>
			</section>
		<!-- End Copyright -->
		<?php endif; ?>
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>build/bower_components/jquery/dist/jquery.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/what-input/dist/what-input.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/foundation-sites/dist/js/foundation.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/fancybox/dist/jquery.fancybox.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/wow/dist/wow.min.js"></script>
			<script src="<?php echo $path; ?>build/app.js" type="text/javascript"></script>
		<!-- End Main Scripts -->
	</body>
</html>